use std::fmt;

const AND_OP: char = '+';
const OR_OP: char = '/';
const NOT_OP: char = '-';

#[derive(Debug, PartialEq, Eq)]
pub enum Token<'t> {
    And,
    Or,
    Not,
    Ident(&'t str),
}

impl<'t> fmt::Display for Token<'t> {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        match self {
            Token::And => write!(f, "+"),
            Token::Or => write!(f, "/"),
            Token::Not => write!(f, "-"),
            Token::Ident(s) => write!(f, "{}", s),
        }
    }
}

impl<'t> From<&'t str> for Token<'t> {
    fn from(s: &'t str) -> Self {
        match s {
            "+" => Token::And,
            "/" => Token::Or,
            "-" => Token::Not,
            _ => Token::Ident(s),
        }
    }
}

#[derive(Debug)]
pub struct ParseError {
    chunk_index: usize,
    str_index: usize,
}

pub fn tokenize<'t>(filter_text: &'t [String]) -> Result<Vec<Token<'t>>, ParseError> {
    let mut tokens = Vec::new();

    for chunk in filter_text {
        let mut chunk = Some(chunk.as_str());
        while let Some(c) = chunk {
            let (token_str, c) = grab_token(c);
            let token = Token::from(token_str);
            tokens.push(token);

            // keep the remainder of the string to grab another token next loop
            chunk = c;
        }
    }

    return Ok(tokens);
}

/// Splits off the first token from the text. If there's any
/// text remaining, returns that as the second tuple value.
fn grab_token<'t>(mut text: &'t str) -> (&'t str, Option<&'t str>) {
    let mut chars = text.char_indices();
    let mut in_ident = false;
    let split_index = loop {
        match chars.next() {
            Some((i, c)) => if let AND_OP | OR_OP | NOT_OP = c {
                break i + !in_ident as usize
            } else {
                in_ident = true;
            },
            None => break text.len()
        }
    };

    let (left, right) = text.split_at(split_index);
    let right = if right.len() > 0 {
        Some(right)
    } else {
        None
    };

    return (left, right);
}

#[derive(Debug, PartialEq, Eq)]
pub enum TreeError {
    /// Indicates that there were extra operators or terms
    /// in the filter that didn't descend from the root. The usize
    /// indicates the index of the first trailing token.
    TrailingExpression(usize),

    /// Indicates that the filter description ended without ever
    /// providing the operand for one of the operations.
    MissingOperand,
}

#[derive(Debug, PartialEq)]
pub struct ValidFilter<'t>(Vec<Token<'t>>);

pub fn validate(tokens: Vec<Token<'_>>) -> Result<ValidFilter<'_>, TreeError> {
    fn validate_child<'b, 'a>(tokens: &'b [Token<'a>]) -> Result<&'b [Token<'a>], TreeError> {
        use Token::*;

        let (front, rest) = tokens.split_first()
            .ok_or(TreeError::MissingOperand)?;

        return match front {
            Ident(_) => Ok(rest),
            Not      => validate_child(rest),
            And | Or => {
                let right_operand = validate_child(rest)?;
                validate_child(right_operand)
            }
        }
    }

    if tokens.is_empty() {
        return Ok(ValidFilter(tokens));
    }

    let orig_len = tokens.len();
    return match validate_child(tokens.as_slice()) {
        Ok(&[]) => Ok(ValidFilter(tokens)),
        Ok(remaining) => Err(TreeError::TrailingExpression(orig_len - remaining.len())),
        Err(e) => Err(e),
    }
}

/// Returns whether or not the given tag passes the tokenized filter.
/// Panics if the filter is not a valid tree (as determined by is_valid_tree).
pub fn passes_tag(tag: &str, filter: &ValidFilter<'_>) -> bool {
    fn consume_filter<'a, 't>(tag: &str, filter: &'a [Token<'t>]) -> (bool, &'a [Token<'t>]) {
        use Token::*;

        // unwrap is safe in this context assuming the user properly called
        // is_valid_tree on the input
        let (front, rest) = filter.split_first().unwrap();
        return match front {
            Ident(ident) => (&tag == ident, rest),
            And => {
                let (left_result, right_operand) = consume_filter(tag, rest);
                let (right_result, rest) = consume_filter(tag, right_operand);
                (left_result && right_result, rest)
            },
            Or => {
                let (left_result, right_operand) = consume_filter(tag, rest);
                let (right_result, rest) = consume_filter(tag, right_operand);
                (left_result || right_result, rest)
            },
            Not => {
                let (passes, rest) = consume_filter(tag, rest);
                (!passes, rest)
            }
        };
    }

    let filter = filter.0.as_slice();
    if filter.len() == 0 {
        return true; // empty filters pass everything
    }

    return consume_filter(tag, filter).0;
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn tokenize_nothing() {
        let tokens = tokenize(&[]).unwrap();
        assert_eq!(tokens, []);
    }

    #[test]
    fn tokenize_and() {
        let chunks = ["+".to_string()];
        let tokens = tokenize(&chunks).unwrap();
        assert_eq!(tokens, [Token::And]);
    }

    #[test]
    fn tokenize_each_token_type() {
        let chunks: Vec<_> = ["+", "/", "-", "identifier"].iter().map(|&s| s.into()).collect();
        let tokens = tokenize(&chunks).unwrap();
        assert_eq!(tokens, [Token::And, Token::Or, Token::Not, Token::Ident("identifier")]);
    }

    #[test]
    fn tokenize_squished_tokens() {
        let chunks = ["+-/identifier".to_string()];
        let tokens = tokenize(&chunks).unwrap();
        assert_eq!(tokens, [Token::And, Token::Not, Token::Or, Token::Ident("identifier")]);
    }

    #[test]
    fn tokenize_several_squished_tokens() {
        use Token::*;

        let chunks: Vec<_> = ["+identifier", "-other+another/", "breakfast", "hello/"].iter().map(|&s| s.into()).collect();
        let tokens = tokenize(chunks.as_slice()).unwrap();

        assert_eq!(tokens, [
            And,
            Ident("identifier"),
            Not,
            Ident("other"),
            And,
            Ident("another"),
            Or,
            Ident("breakfast"),
            Ident("hello"),
            Or
        ]);
    }

    #[test]
    fn valid_empty_tree() {
        let tokens = vec![];
        let validation = validate(tokens);
        assert!(validation.is_ok());
    }

    #[test]
    fn valid_single_identifier() {
        let tokens = vec![Token::Ident("breakfast")];
        let validation = validate(tokens);
        assert!(validation.is_ok());
    }

    #[test]
    fn valid_not_operator() {
        let tokens = vec![Token::Not, Token::Ident("breakfast")];
        let validation = validate(tokens);
        assert!(validation.is_ok());
    }

    #[test]
    fn valid_and_operator() {
        let tokens = vec![Token::And, Token::Ident("breakfast"), Token::Ident("vegetarian")];
        let validation = validate(tokens);
        assert!(validation.is_ok());
    }

    #[test]
    fn valid_or_operator() {
        let tokens = vec![Token::Or, Token::Ident("breakfast"), Token::Ident("vegetarian")];
        let validation = validate(tokens);
        assert!(validation.is_ok());
    }

    #[test]
    fn valid_complex_example() {
        use Token::*;

        let tokens = vec![Not, Or, Ident("breakfast"), And, Ident("vegan"), Not, Ident("ingredient")];
        let validation = validate(tokens);
        assert!(validation.is_ok());
    }

    #[test]
    fn invalid_trailing_operand() {
        use Token::*;

        let tokens = vec![Not, Or, Ident("breakfast"), And, Ident("vegan"), Not, Ident("ingredient"), Ident("happiness")];
        let validation = validate(tokens);
        assert_eq!(validation, Err(TreeError::TrailingExpression(7)));
    }

    #[test]
    fn invalid_missing_not_operand() {
        use Token::*;

        let tokens = vec![Not, Or, Ident("breakfast"), And, Ident("vegan"), Not];
        let validation = validate(tokens);
        assert_eq!(validation, Err(TreeError::MissingOperand));
    }

    #[test]
    fn invalid_missing_or_operand() {
        use Token::*;

        let tokens = vec![Not, Or, And, Ident("vegan"), Not, Ident("ingredient")];
        let validation = validate(tokens);
        assert_eq!(validation, Err(TreeError::MissingOperand));
    }

    #[test]
    fn empty_filter_passes() {
        let tokens = vec![];
        let filter = validate(tokens).unwrap();
        assert!(passes_tag("breakfast", &filter));
    }

    #[test]
    fn exact_ident_passes() {
        use Token::*;

        let tokens = vec![Ident("breakfast")];
        let filter = validate(tokens).unwrap();
        assert!(passes_tag("breakfast", &filter));
    }

    #[test]
    fn wrong_ident_fails() {
        use Token::*;

        let tokens = vec![Ident("dinner")];
        let filter = validate(tokens).unwrap();
        assert!(!passes_tag("breakfast", &filter));
    }

    #[test]
    fn not_wrong_ident_passes() {
        use Token::*;

        let tokens = vec![Not, Ident("dinner")];
        let filter = validate(tokens).unwrap();
        assert!(passes_tag("breakfast", &filter));
    }

    #[test]
    fn not_exact_ident_fails() {
        use Token::*;

        let tokens = vec![Not, Ident("breakfast")];
        let filter = validate(tokens).unwrap();
        assert!(!passes_tag("breakfast", &filter));
    }

    #[test]
    fn idempotent_and_passes() {
        use Token::*;

        let tokens = vec![And, Ident("breakfast"), Ident("breakfast")];
        let filter = validate(tokens).unwrap();
        assert!(passes_tag("breakfast", &filter));
    }

    #[test]
    fn idempotent_or_passes() {
        use Token::*;

        let tokens = vec![Or, Ident("breakfast"), Ident("breakfast")];
        let filter = validate(tokens).unwrap();
        assert!(passes_tag("breakfast", &filter));
    }

    #[test]
    fn complex_filter_passes() {
        use Token::*;

        let tokens = vec![Not, Or, Ident("breakfast"), And, Ident("vegan"), Not, Ident("ingredient")];
        let filter = validate(tokens).unwrap();
        assert!(passes_tag("ingredient", &filter));
    }

    #[test]
    fn complex_filter_fails() {
        use Token::*;

        let tokens = vec![Not, Or, Ident("breakfast"), And, Ident("vegan"), Not, Ident("ingredient")];
        let filter = validate(tokens).unwrap();
        assert!(!passes_tag("vegan", &filter));
    }
}
