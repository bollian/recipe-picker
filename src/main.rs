use std::collections::HashMap;

mod filter;

struct Recipe {
    book: &'static str,
    chapter: &'static str,
    chapter_page: i32,

    name: &'static str,
    page: i32,
}

struct UnfilledChapter {
    book: &'static str,
    title: &'static str,
    page: i32,

    unfilled_recipes: i32,
}

macro_rules! describe_book {
    ($recipes:ident, $unfilled_chapters:ident, $tags:ident;
     $book_title:literal: [
        $($chapter_title:literal(page: $chapter_page:literal, count: $recipe_count:literal)$(: [
            $($recipe_name:literal $recipe_page:literal $(($($tag:ident)*))?),*
        ])?),*
     ]) => {
        // prevents needless warnings that can crop up when the unsignedness
        // of a comparison renders it always true or false
        #[allow(unused_comparisons)]
        {
        $($({
            let mut new_recipes = vec![$(
                Recipe {
                    book: $book_title,
                    chapter: $chapter_title,
                    chapter_page: $chapter_page,
                    name: $recipe_name,
                    page: $recipe_page,
                }
            ),*];

            if $recipe_count > new_recipes.len() {
                $unfilled_chapters.push(UnfilledChapter {
                    book: $book_title,
                    title: $chapter_title,
                    page: $chapter_page,
                    unfilled_recipes: ($recipe_count - new_recipes.len()) as i32,
                });
            } else if $recipe_count < new_recipes.len() && $recipe_count != 0 {
                eprintln!("Chapter '{}' from book '{}' has more recipes listed than it contains.", $chapter_title, $book_title);
            }

            $recipes.append(&mut new_recipes);
        })*)?
    }};
}

fn main() {
    use rand::distributions::Distribution;

    let mut recipes = Vec::new();
    let mut unfilled_chapters = Vec::new();
    let mut tags: HashMap<&'static str, Vec<usize>> = HashMap::new();

    describe_book!(recipes, unfilled_chapters, tags;
    "The Food Lab": [
        "Eggs, Dairy, and the Science of Breakfast"(page: 83, count: 38): [
            "Foolproof Soft-Boiled Eggs" 101 (breakfast),
            "Foolproof Hard-Boiled Eggs" 102 (breakfast),
            "Perfect Poached Eggs" 107 (breakfast),
            "Foolproof Hollandaise Sauce" 111 (breakfast),
            "Eggs Benedict" 112 (breakfast),
            "Eggs Florentine" 113 (breakfast),
            "Extra-Crispy Sunny-Side-Up Eggs" 116 (breakfast),
            "Light and Fluffy Scrambled Eggs" 121 (breakfast),
            "Creamy Scrambled Eggs" 122 (breakfast),
            "Easy Homemade Crème Fraîche" 123 (breakfast ingredient),
            "Diner-Style Ham and Cheese Omelet" 125 (breakfast),
            "Diner-Style Mushroom, Pepper, and Onion Omelet" 126 (breakfast vegetarian),
            "Diner-Style Asparagus, Shallot, and Goat Cheese Omelet" 126 (breakfast vegetarian),
            "Tender Fancy-Pants Omelet" 128 (breakfast),
            "Crispy Fried Bacon" 133 (breakfast),
            "Crispy Oven-Fried Bacon for a Crowd" 133 (breakfast),
            "Basic Crispy Potato Cake (aka Rösti)" 135 (breakfast vegetarian),
            "Crispy Potato, Onion, and Mushroom Cake (aka Rösti)" 136 (breakfast vegetarian),
            "Potato Hash with Peppers and Onions" 140 (breakfast vegetarian),
            "Potato and Corned Beef Hash" 140 (breakfast),
            "Basic Dry Pancake Mix" 149 (breakfast),
            "Light and Fluffy Buttermilk Pancakes" 150 (breakfast vegetarian),
            "Blueberry Pancakes" 150 (breakfast vegetarian),
            "Fresh Ricotta in 5 Minutes or Less" 154 (breakfast vegetarian),
            "Warm Ricotta with Olive Oil and Lemon Zest" 154 (breakfast vegetarian),
            "Lemon Ricotta Pancakes" 155 (breakfast vegetarian),
            "Basic Quick Waffles" 157 (breakfast vegetarian),
            "Orange-Scented Waffles" 157 (breakfast vegetarian),
            "Maple Bacon Waffles" 157 (breakfast),
            "Super-Flaky Buttermilk Biscuits" 163 (breakfast),
            "Cheddar Cheese and Scallion Biscuits" 163 (breakfast),
            "Bacon Parmesan Biscuits" 163 (breakfast),
            "Flaky Scones" 164 (breakfast),
            "Creamy Sausage Gravy" 164 (breakfast),
            "Easy Cream Biscuits" 165 (breakfast),
            "Cream Scones" 165 (breakfast),
            "The World's Most Awesome Sticky Buns" 167 (breakfast),
            "Homemade Hot Chocolate Mix" 173 (breakfast)
        ],
        "Soups, Stews, and the Science of Stock"(page: 175, count: 29): [
            "Quick Chicken Stock" 187
        ],
        "Steaks, Chops, Chicken, Fish, and the Science of Fast-Cooking Foods"(page: 277, count: 47),
        "Blanching, Searing, Brasing, Glazing, Roasting, and the Science of Vegetables"(page: 403, count: 43),
        "Balls, Loaves, Links, Burgers, and the Science of Ground Meat"(page: 481, count: 22): [
            "Basic Homemade Sausage" 503,
            "Seasoning Mix for Garlic Sausage" 505,
            "Seasoning Mi for Sweet or Hot Italian Sausage" 505,
            "Seasoning Mix for Bratwurst-Style Sausage" 505,
            "Seasoning Mix for Mexican Chorizo" 506,
            "Seasoning Mix for Merguez-Style Lamb Sausage" 506,
            "Maple-Sage Breakfast Sausage" 507,
            "Garlic Sausage with Lentils" 514,
            "Grilled Italian Sausage with Onions and Peppers" 515,
            "Grilled or Pan-Roasted Merguez with Yogurt, Mint, and Moroccan Salad" 516,
            "Easy Grilled Naan-Style Flatbread" 517,
            "Even Easier Grilled Flatbread" 518,
            "Grilled or Pan-Roasted Bratwurst with Beer, Mustard, and Sauerkraut" 520,
            "Grilled or Pan-Roasted Mexican Chorizo with Spicy Tomato-Caper Sauce" 521,
            "Grilled or Pan-Roasted hot Dogs with Sauerkraut" 523,
            "All-American Meat Loaf" 531,
            "Leftover Meat Loaf Sandwich" 534,
            "Tender Italian Meatballs with Rich Tomato Sauce" 538,
            "Pork Meatballs with Mushroom Cream Sauce" 542,
            "Classic Diner-Style Smashed Cheeseburgers" 552,
            "Fry Sauce" 553,
            "Pub-Style Thick and Juicy Cheeseburgers" 558
        ],
        "Chickens, Turkeys, Prime Rib, and the Science of Roasts"(page: 561, count: 34),
        "Tomato Sauce, Macaroni, and the Science of Pasta"(page: 669, count: 39),
        "Greens, Emulsions, and the Science of Salads"(page: 761, count: 36),
        "Batter, Breadings, and the Science of Frying"(page: 845, count: 14): [
            "Extra-Crunchy Southern Fried Chicken" 871,
            "Extra-Crunchy Chicken-Fried Steak with Cream Gravy" 873,
            "Extra-Crunchy Fried Chicken Sandwiches" 876,
            "Chicken Parmesan" 878,
            "Eggplant Parmesan Casserole" 882,
            "Beer-Battered Fried Cod" 892,
            "Extra-Tangy Tartar Sauce" 894,
            "Fried Fish Sandwiches with Creamy Slaw and Tartar Sauce" 896,
            "Foolproof Onion Rings" 900,
            "Tempura Vegetables and/or Shrimp Honey-Miso Mayonnaise" 902,
            "Thin and Crispy French Fries" 910,
            "The Ultimate Quintuple-Cooked Thick and Crisp Steak Fries" 911,
            "Crunchy Oven Fries" 914
        ]
    ]);

    let unfilled_total: i32 = unfilled_chapters.iter()
        .map(|c| c.unfilled_recipes).sum();
    let unfilled_total = unfilled_total as usize;

    let between = rand::distributions::Uniform::new(0, unfilled_total + recipes.len());
    let mut rng = rand::thread_rng();
    let pick = between.sample(&mut rng);

    if pick < recipes.len() {
        let recipe = &recipes[pick];
        println!("{}", recipe.book);
        println!("↳ {} (page {})", recipe.chapter, recipe.chapter_page);
        println!("  ↳ {} (page {})", recipe.name, recipe.page);
    } else {
        let (chapter, recipe_num) = nth_unfilled_recipe(&unfilled_chapters[..], pick - recipes.len());

        let number_text = match recipe_num % 100 {
            11 | 12 | 13 => format!("{}th", recipe_num),
            _ => match recipe_num % 10 {
                1 => format!("{}st", recipe_num),
                2 => format!("{}nd", recipe_num),
                3 => format!("{}rd", recipe_num),
                _ => format!("{}th", recipe_num)
            }
        };

        println!("{}", chapter.book);
        println!("↳ {} (page {})", chapter.title, chapter.page);
        println!("  ↳ {} recipe", number_text);
    }
}

fn nth_unfilled_recipe(chapters: &[UnfilledChapter], mut n: usize) -> (&UnfilledChapter, usize) {
    use std::convert::TryInto;

    for chapter in chapters {
        let recipe_count: usize = chapter.unfilled_recipes.try_into().unwrap();
        if n < recipe_count {
            return (chapter, n);
        }

        n -= recipe_count;
    }

    panic!("Ran out of chapters");
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn describe_book_single_chapter() {
        let mut recipes = Vec::new();
        let mut unfilled_chapters = Vec::new();
        let mut tags: HashMap<&'static str, Vec<usize>> = HashMap::new();

        describe_book!(recipes, unfilled_chapters, tags;
        "The Food Lab": [
            "Eggs, Dairy, and the Science of Breakfast"(page: 83, count: 38): [
                "Foolproof Soft-Boiled Eggs" 101 (breakfast),
                "Foolproof Hard-Boiled Eggs" 102 (breakfast)
            ]
        ]);

        assert_eq!(recipes.len(), 2);
        assert_eq!(unfilled_chapters.len(), 1);

        assert_eq!(recipes[0].book, "The Food Lab");
        assert_eq!(recipes[0].chapter, "Eggs, Dairy, and the Science of Breakfast");
        assert_eq!(recipes[0].chapter_page, 83);
        assert_eq!(recipes[0].name, "Foolproof Soft-Boiled Eggs");
        assert_eq!(recipes[0].page, 101);

        assert_eq!(recipes[1].name, "Foolproof Hard-Boiled Eggs");
        assert_eq!(recipes[1].page, 102);

        assert_eq!(unfilled_chapters[0].book, "The Food Lab");
        assert_eq!(unfilled_chapters[0].title, "Eggs, Dairy, and the Science of Breakfast");
        assert_eq!(unfilled_chapters[0].page, 83);
        assert_eq!(unfilled_chapters[0].unfilled_recipes, 36);
    }

    #[test]
    fn describe_book_multiple_chapters() {
        let mut recipes = Vec::new();
        let mut unfilled_chapters = Vec::new();
        let mut tags: HashMap<&'static str, Vec<usize>> = HashMap::new();

        describe_book!(recipes, unfilled_chapters, tags;
        "The Food Lab": [
            "Eggs, Dairy, and the Science of Breakfast"(page: 83, count: 38): [
                "Foolproof Soft-Boiled Eggs" 101,
                "Foolproof Hard-Boiled Eggs" 102
            ],
            "Soups, Stews, and the Science of Stock"(page: 175, count: 29): [
                "Quick Chicken Stock" 187
            ]
        ]);

        assert_eq!(recipes.len(), 3);
        assert_eq!(unfilled_chapters.len(), 2);

        assert_eq!(recipes[0].chapter, "Eggs, Dairy, and the Science of Breakfast");
        assert_eq!(recipes[1].chapter, "Eggs, Dairy, and the Science of Breakfast");
        assert_eq!(recipes[2].chapter, "Soups, Stews, and the Science of Stock");

        assert_eq!(unfilled_chapters[0].title, "Eggs, Dairy, and the Science of Breakfast");
        assert_eq!(unfilled_chapters[1].title, "Soups, Stews, and the Science of Stock");

        assert_eq!(unfilled_chapters[0].unfilled_recipes, 36);
        assert_eq!(unfilled_chapters[1].unfilled_recipes, 28);
    }

    #[test]
    fn describe_book_empty() {
        let recipes: Vec<Recipe> = Vec::new();
        let unfilled_chapters: Vec<UnfilledChapter> = Vec::new();
        let mut tags: HashMap<&'static str, Vec<usize>> = HashMap::new();

        describe_book!(recipes, unfilled_chapters, tags;
        "Boring Cook Book": []
        );

        assert_eq!(recipes.len(), 0);
        assert_eq!(unfilled_chapters.len(), 0);
    }

    #[test]
    fn describe_book_empty_chapter() {
        let mut recipes: Vec<Recipe> = Vec::new();
        let mut unfilled_chapters = Vec::new();
        let mut tags: HashMap<&'static str, Vec<usize>> = HashMap::new();

        describe_book!(recipes, unfilled_chapters, tags;
        "How to Cook Everything": [
            "Cool Chapter Bro"(page: 101, count: 0): []
        ]);

        assert_eq!(recipes.len(), 0);
        assert_eq!(unfilled_chapters.len(), 0);
    }

    #[test]
    fn describe_book_empty_unfilled_chapter() {
        let mut recipes: Vec<Recipe> = Vec::new();
        let mut unfilled_chapters = Vec::new();
        let mut tags: HashMap<&'static str, Vec<usize>> = HashMap::new();

        describe_book!(recipes, unfilled_chapters, tags;
        "How to Cook Everything": [
            "Cool Chapter Bro"(page: 101, count: 25): []
        ]);

        assert_eq!(recipes.len(), 0);
        assert_eq!(unfilled_chapters.len(), 1);

        assert_eq!(unfilled_chapters[0].book, "How to Cook Everything");
        assert_eq!(unfilled_chapters[0].title, "Cool Chapter Bro");
        assert_eq!(unfilled_chapters[0].page, 101);
        assert_eq!(unfilled_chapters[0].unfilled_recipes, 25);
    }
}
